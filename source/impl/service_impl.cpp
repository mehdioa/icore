#include "service_impl.h"
#include "logger.h"
#include <asio/signal_set.hpp>

struct Service::Impl::IOS {
    IOS(int n, Service::Impl* p)
        : count(n)
        , parent(p)
    {
    }

    asio::io_service ios;
    int count;
    std::vector<std::unique_ptr<std::thread>> threads;
    std::unique_ptr<asio::signal_set> signal_set;
    Service::Impl* parent;

    ~IOS()
    {
        stop();
    }

    void start()
    {
        signal_set.reset(new asio::signal_set(ios, SIGINT));
        signal_set->async_wait(std::bind(&Service::Impl::signal_handler, parent,
            std::placeholders::_1,
            std::placeholders::_2));

        for (int i = 0; i < count; ++i) {
            std::unique_ptr<std::thread> t(new std::thread([this, i]() {
                ios.run();
            }));
            threads.push_back(std::move(t));
        }

        Info("Worker started with {} threads", count);
    }

    void stop()
    {
        ios.stop();
        Info("IOS stopped");

        for (auto& t : threads) {
            if (t->joinable()) {
                t->join();
            }
        }

        threads.clear();
        Info("IOS threads joined");
    }
};

Service::Impl::Impl()
{
}

Service::Impl::Impl(const Service::Impl& )
{
    Error("Implement me");
}
void Service::Impl::stop() noexcept
{
    std::lock_guard<std::mutex> lock(m_runningMtx);

    if (m_isRunning.load()) {

        for (auto& x : m_ioss) {
            x->ios.stop();
            Debug("Worker stopped");
        }

        Info("All workers stopped");
        m_isRunning.store(false);
    }
} // Service::Impl::stop

void Service::Impl::runThreads() noexcept
{
    m_isRunning.store(true);

    for (auto& x : m_ioss) {
        x->start();
    }
} // Service::Impl::runThreads

void Service::Impl::joinThreads() noexcept
{
    std::mutex fake;
    std::unique_lock<std::mutex> ul(fake);
    m_runningCV.wait(ul);
    stop();
}

void Service::Impl::signal_handler(const asio::error_code& error,
    const int signal_number)
{
    std::lock_guard<std::mutex> lock(m_runningMtx);

    if (m_isRunning.load()) {
        Warn("Received signal {} with error {}", signal_number, error.message());
        m_runningCV.notify_all();
    }
} // Service::Impl::signal_handler

asio::io_service& Service::Impl::service(size_t n) noexcept
{
    return m_ioss.at(n)->ios;
}

bool Service::Impl::setParameter(const pugi::xml_node& node) noexcept
{
    std::string node_name(node.name());
    Info("setParameter called for {}", node_name);

    if (node_name == "Workers") {
        for (auto sub_node = node.first_child(); sub_node; sub_node = sub_node.next_sibling()) {
            auto threads = sub_node.text().as_int();
            Info("Adding worker with {} threads", threads);
            m_ioss.push_back(std::make_shared<Impl::IOS>(threads, this));
        }
    }

    return false;
} // Service::Impl::setParameter
