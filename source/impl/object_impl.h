#pragma once

#include "object.h"
#include <atomic>
#include <map>
#include <mutex>
#include <pugixml.hpp>
#include <unordered_map>
#include <vector>

struct ICore::Object::Impl {
    Impl();
    Impl(const Impl& rhs);
    ~Impl();

    id_type id() const;
    shared_type child(const id_type& id) noexcept;

    void setParameter(const pugi::xml_node& node) noexcept;
    void setArrayParameter(const pugi::xml_node& node);
    shared_type createChild(const pugi::xml_node& node);

    void start() noexcept;
    void stop() noexcept;

    void addChild(shared_type child, pointer_type parent) noexcept;

    shared_type releaseChild(id_type id) noexcept;

    void setParent(pointer_type parent) noexcept;

    std::string m_name;
    id_type m_id;
    pointer_type m_parent;
    std::atomic_bool m_isRunning;
    std::unordered_map<id_type, IObject::shared_type> m_children;
    id_type m_lastChildId = 0;
    std::mutex m_childrenGuard;
};
