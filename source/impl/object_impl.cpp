#include "object_impl.h"
#include "logger.h"

using namespace ICore;
using id_type = ICore::IObject::id_type;
using shared_type = ICore::IObject::shared_type;

Object::Impl::Impl()
    : m_id(0)
{
    m_isRunning.store(false);
    Debug("Object 0 created");
}

Object::Impl::Impl(const ICore::Object::Impl& rhs)
    : m_name(rhs.m_name)
    , m_id(rhs.m_id)
    , m_parent(rhs.m_parent)
{
    m_isRunning.store(rhs.m_isRunning.load());

    Error("Implement me");
    //    std::lock_guard<std::mutex> lock(rhs.m_childrenGuard);
}

Object::Impl::~Impl()
{
    std::lock_guard<std::mutex> lock(m_childrenGuard);
    m_children.clear();
}

id_type Object::Impl::id() const
{
    return m_id;
}
shared_type Object::Impl::child(const id_type& id) noexcept
{
    try {
        std::lock_guard<std::mutex> lock(m_childrenGuard);
        return m_children.at(id);
    } catch (const std::out_of_range&) {
        Warn("Object {} has no child of id {}", m_id, id);
        return nullptr;
    }
}

void Object::Impl::setParameter(const pugi::xml_node& node) noexcept
{
    std::string node_name(node.name());
    Info("setParameter called for {}", node_name);

    if (node_name == "Name") {
        m_name = std::string(node.text().as_string());
        Debug("Object name is {}", m_name);
    } else {
        Warn("setParameter called for parameter {}",
            node_name);
    }
}

void Object::Impl::setArrayParameter(const pugi::xml_node& node)
{
    Error("Default implementation of setArrayParameter called for {}", node.name());
}

shared_type Object::Impl::createChild(const pugi::xml_node& node)
{
    Error("Default implementation of createChild called for {}", node.name());
    return nullptr;
}

void Object::Impl::start() noexcept
{
    std::lock_guard<std::mutex> lock(m_childrenGuard);
    for (auto& child : m_children) {
        child.second->start();
    }
}

void Object::Impl::stop() noexcept
{
    std::lock_guard<std::mutex> lock(m_childrenGuard);
    for (auto& child : m_children) {
        child.second->stop();
    }
}

void Object::Impl::addChild(shared_type child, pointer_type parent) noexcept
{
    if (child) {
        child->setParent(parent);
        std::lock_guard<std::mutex> lock(m_childrenGuard);
        child->setId(++m_lastChildId);
        m_children.emplace(child->id(), child);
        Trace("Child {} added to parent {}", child->id(), id());
    } else {
        Warn("Cannot add null child");
    }
}

shared_type Object::Impl::releaseChild(id_type id) noexcept
{
    try {
        std::lock_guard<std::mutex> lock(m_childrenGuard);
        auto& child = m_children.at(id);
        m_children.erase(id);
        return child;
    } catch (const std::out_of_range& e) {
        Warn("Exception {}\nParent {} has no child of id {}", e.what(), m_id, id);
        return nullptr;
    }
}

void Object::Impl::setParent(pointer_type parent) noexcept
{
    m_parent = parent;
}
