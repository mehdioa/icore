#pragma once

#include "service.h"

#include <asio/signal_set.hpp>
#include <condition_variable>
#include <memory>
#include <mutex>
#include <thread>
#include <vector>

using namespace ICore;

struct Service::Impl {
    struct IOS;

    Impl();
    Impl(const Impl& );

    void stop() noexcept;

    void runThreads() noexcept;
    void joinThreads() noexcept;

    void signal_handler(const asio::error_code& error, const int signal_number);

    asio::io_service& service(size_t n) noexcept;

    bool setParameter(const pugi::xml_node& node) noexcept;

    std::vector<std::shared_ptr<IOS>> m_ioss;

    std::mutex m_runningMtx;
    std::condition_variable m_runningCV;
    std::atomic_bool m_isRunning;
};
