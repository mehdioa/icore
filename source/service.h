#pragma once

#include "class_def.h"
#include "iservice.h"
#include "object.h"

namespace ICore {
class Service;
}

class ICore::Service : public ICore::Object,
                       public ICore::IService,
                       public std::enable_shared_from_this<ICore::Service> {
    DEC_CMA(Service)

    explicit Service();
    Service(const Service& service);

    void readConfig(const std::string& config_file) noexcept override;

    asio::io_service& service(size_t n) noexcept override;


    void setParameter(const pugi::xml_node& node) noexcept override;
    void start() noexcept override;
    void stop() noexcept override;
};
