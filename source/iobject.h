#pragma once

#include <memory>
#include <pugixml.hpp>

namespace ICore {

class IObject {
public:
    typedef std::shared_ptr<IObject> shared_type;
    typedef std::weak_ptr<IObject> weak_type;
    typedef std::unique_ptr<IObject> unique_type;
    typedef IObject* pointer_type;
    typedef unsigned long long id_type;

    virtual ~IObject() = default;

    /**
     * @brief id The id of an object is unique
     * @return The id of the object
     */
    virtual id_type id() const noexcept = 0;

    /**
     * @brief setId Set the unique id of the object
     * @param id The object id
     */
    virtual void setId(id_type id) noexcept = 0;

    /**
     * @brief child Get access to a child of the object
     * @param id The id of the child
     * @return The child with id #id
     */
    virtual shared_type child(const id_type& id) noexcept = 0;

    virtual void setParameter(const pugi::xml_node& node) noexcept = 0;

    virtual shared_type createChild(const pugi::xml_node& node) noexcept = 0;

    virtual void start() noexcept = 0;

    virtual void stop() noexcept = 0;

    virtual std::string name() const = 0;

    virtual void config(const pugi::xml_node& node) noexcept = 0;

    virtual void addChild(shared_type child) noexcept = 0;

    virtual shared_type releaseChild(id_type id) noexcept = 0;

    virtual void setParent(pointer_type parent) noexcept = 0;
};
}
