#include "object.h"
#include "impl/object_impl.h"
#include "logger.h"
#include <atomic>
#include <map>
#include <mutex>

using namespace ICore;
using id_type = ICore::IObject::id_type;

Object::Object(const Object& object)
    : std::enable_shared_from_this<Object>()
    , pImpl(std::make_unique<Impl>(*object.pImpl))
{
}

Object::Object(Object&& t) = default;

Object& Object::operator=(const Object& rhs)
{
    pImpl.reset(new Impl(*rhs.pImpl));
    return *this;
}

Object& Object::operator=(Object&& rhs) = default;

Object::~Object() = default;

Object::Object()
    : pImpl(std::make_unique<Impl>())
{
}
id_type Object::id() const noexcept
{
    return pImpl->id();
}

void Object::setId(IObject::id_type id) noexcept
{
    pImpl->m_id = id;
}
IObject::shared_type Object::child(const id_type& id) noexcept
{
    return pImpl->child(id);
}

void Object::config(const pugi::xml_node& node) noexcept
{
    for (pugi::xml_node sub_node = node.first_child(); sub_node; sub_node = sub_node.next_sibling()) {
        std::string attribute(sub_node.attribute("type").value());
        if (attribute == "node") {
            auto child = createChild(sub_node);
            addChild(child);
            child->config(sub_node);
        } else {
            setParameter(sub_node);
        }
    }
}

void Object::addChild(IObject::shared_type child) noexcept
{
    pImpl->addChild(child, this);
}

IObject::shared_type Object::releaseChild(id_type id) noexcept
{
    return pImpl->releaseChild(id);
}

void Object::setParent(pointer_type parent) noexcept
{
    pImpl->setParent(parent);
}

void Object::setParameter(const pugi::xml_node& node) noexcept
{
    pImpl->setParameter(node);
}

IObject::shared_type Object::createChild(const pugi::xml_node& node) noexcept
{
    Warn("Default createChild called for {}", node.name());
    return nullptr;
}

void Object::start() noexcept
{
    pImpl->start();
}
void Object::stop() noexcept
{
    pImpl->stop();
}

std::string Object::name() const
{
    return pImpl->m_name;
}
