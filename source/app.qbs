import qbs
import "../../preset.js" as Preset

Product {
    name: Preset.setName(qbs.buildVariant, "icore")
    type: project.LIB_TYPE
    files: [
        "*.cpp",
        "impl/*.cpp",
    ]

    cpp.cxxLanguageVersion: project.cxxLanguageVersion
    cpp.warningLevel: project.warningLevel

    Depends { name: "cpp" }

    cpp.libraryPaths: [project.LIB_DIR]

    cpp.dynamicLibraries: {
        var sl = ["pugixml"]
        if (qbs.targetOS.contains("unix"))  {
            sl.push("pthread")
        }
        return sl
    }

    cpp.includePaths: {
        var ip = ["."]
        if (qbs.targetOS.contains("windows")) {
            ip.push(project.INC_DIR)
        }
        return ip
    }

    cpp.defines: {
        var defs = ["RAPIDJSON_HAS_STDSTRING", "ASIO_STANDALONE"]
        if (qbs.targetOS.contains("windows")) {
            defs.push("WIN32")
        }

        return defs
    }

    Group {
     condition: qbs.targetOS.contains("unix") && !qbs.targetOS.contains("osx")
     qbs.install: true
     qbs.installDir: "local/lib"
     fileTagsFilter:  product.type
    }
    Group {
        condition: qbs.targetOS.contains("unix") && !qbs.targetOS.contains("osx")
        qbs.install: true
        qbs.installDir: "local/include/icore"
        files: ["*.h", "*.hpp"]
    }
}
