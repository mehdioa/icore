#include "logger.h"
#include "utils.h"
#include <pugixml.hpp>

#ifdef WIN32
#define DEFINE_CONSOLEV2_PROPERTIES
#define ENABLE_VIRTUAL_TERMINAL_PROCESSING 0x0200

// System headers
#include <windows.h>

// Standard library C-style
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#endif // ifdef WIN32

#define RESET "\x1b[0m"
#define CLEAR "\x1b[2J"
#define BLACK "\x1b[30m"
#define RED "\x1b[31m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define BLUE "\x1b[34m"
#define MAGENTA "\x1b[35m"
#define CYAN "\x1b[36m"
#define WHITE "\x1b[37m"
#define BOLDBLACK "\x1b[1m\x1b[30m"
#define BOLDRED "\x1b[1m\x1b[31m"
#define BOLDGREEN "\x1b[1m\x1b[32m"
#define BOLDYELLOW "\x1b[1m\x1b[33m"
#define BOLDBLUE "\x1b[1m\x1b[34m"
#define BOLDMAGENTA "\x1b[1m\x1b[35m"
#define BOLDCYAN "\x1b[1m\x1b[36m"
#define BOLDWHITE "\x1b[1m\x1b[37m"
#define BLINKRED "\x1b[31m\x1b[5m"

LogPtr Trace_ = spdlog::get("trace");
LogPtr Debug_ = spdlog::get("debug");
LogPtr Info_ = spdlog::get("info");
LogPtr Warn_ = spdlog::get("warn");
LogPtr Error_ = spdlog::get("error");
LogPtr Critical_ = spdlog::get("critical");

void config(const std::string& config_file)
{
#ifdef WIN32
    // Set output mode to handle virtual terminal sequences
    HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);

    if (hOut != INVALID_HANDLE_VALUE) {
        //        return GetLastError();
        DWORD dwMode = 0;

        if (GetConsoleMode(hOut, &dwMode)) {
            //            return GetLastError();

            dwMode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
            //            if (!SetConsoleMode(hOut, dwMode))
            //            {
            //                return GetLastError();
            //            }
        }
    }

#endif // ifdef WIN32

    pugi::xml_document doc;

    if (!doc.load_file(config_file.c_str())) {
        exit(EXIT_FAILURE);
    }

    pugi::xml_node logger = doc.child("Logger");

    std::string output(logger.child("Output").text().as_string());
    auto colored_log = logger.child("Colored").text().as_bool();
    auto inverted = logger.child("Inverted").text().as_bool();
    std::string pattern(logger.child("Pattern").text().as_string());
    auto maxFileSize = logger.child("MaxFileSize").text().as_uint();
    auto maxFiles = logger.child("MaxFiles").text().as_uint();
    std::string log_level(logger.child("Level").text().as_string());

    spdlog::level::level_enum level = spdlog::level::off;

    for (int i = spdlog::level::trace; i <= spdlog::level::off; ++i) {
        if ((log_level).compare(spdlog::level::level_names[i]) == 0) {
            level = spdlog::level::level_enum(i);
            break;
        }
    }

    std::string format;

    if (colored_log) {
        auto inv = (inverted ? "\x1b[7m" : "");
        format = pattern + std::string(inv) + std::string("%v\x1b[27m");
    } else {
        format = pattern + std::string("%v");
    }

    if (output == "console") {
        Trace_ = spdlog::stdout_color_mt("trace");
        Debug_ = spdlog::stdout_color_mt("debug");
        Info_ = spdlog::stdout_color_mt("info");
        Warn_ = spdlog::stdout_color_mt("warn");
        Error_ = spdlog::stdout_color_mt("error");
        Critical_ = spdlog::stdout_color_mt("critical");
    } else if (output == "file") {
        Trace_ = spdlog::rotating_logger_mt("trace", "logs/log", maxFileSize, maxFiles);
        Debug_ = spdlog::rotating_logger_mt("debug", "logs/log", maxFileSize, maxFiles);
        Info_ = spdlog::rotating_logger_mt("info", "logs/log", maxFileSize, maxFiles);
        Warn_ = spdlog::rotating_logger_mt("warn", "logs/log", maxFileSize, maxFiles);
        Error_ = spdlog::rotating_logger_mt("error", "logs/log", maxFileSize, maxFiles);
        Critical_ = spdlog::rotating_logger_mt("critical", "logs/log", maxFileSize, maxFiles);
    }

    if (colored_log) {
        Trace_->set_pattern(std::string(GREEN) + format);
        Debug_->set_pattern(std::string(RESET) + format);
        Info_->set_pattern(std::string(BLUE) + format);
        Warn_->set_pattern(std::string(YELLOW) + format);
        Error_->set_pattern(std::string(RED) + format);
        Critical_->set_pattern(std::string(BLINKRED) + format);
    } else {
        Trace_->set_pattern(format);
        Debug_->set_pattern(format);
        Info_->set_pattern(format);
        Warn_->set_pattern(format);
        Error_->set_pattern(format);
        Critical_->set_pattern(format);
    }

    Trace_->set_level(level);
    Debug_->set_level(level);
    Info_->set_level(level);
    Warn_->set_level(level);
    Error_->set_level(level);
    Critical_->set_level(level);
} // config
