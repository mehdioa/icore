#include "service.h"
#include "impl/service_impl.h"
#include "logger.h"

using namespace ICore;

DEF_CMA(Service)

Service::Service()
    : pImpl(std::make_unique<Impl>())
{
}

Service::Service(const Service& service)
    : Object()
    , std::enable_shared_from_this<Service>()
    , pImpl(std::make_unique<Impl>(*service.pImpl))
{
}

void Service::readConfig(const std::string& config_file) noexcept
{
    pugi::xml_document doc;

    if (!doc.load_file(config_file.c_str())) {
        Error("Cannot load config file {}", config_file);
        exit(EXIT_FAILURE);
    }

    auto service = doc.child("Service");

    Object::config(service);
}

void Service::start() noexcept
{
    pImpl->runThreads();
    Object::start();
    pImpl->joinThreads();
}

void Service::stop() noexcept
{
    Object::stop();
    pImpl->stop();
}

asio::io_service& Service::service(size_t n) noexcept
{
    return pImpl->service(n);
}
void Service::setParameter(const pugi::xml_node& node) noexcept
{
    if (!pImpl->setParameter(node)) {
        Object::setParameter(node);
    }
}
