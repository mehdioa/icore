#pragma once

#include <spdlog/fmt/ostr.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

typedef std::shared_ptr<spdlog::logger> LogPtr;

extern LogPtr Trace_;
extern LogPtr Debug_;
extern LogPtr Info_;
extern LogPtr Warn_;
extern LogPtr Error_;
extern LogPtr Critical_;

#define INIT_LOGGERS LogPtr Trace_, Debug_, Info_, Warn_, Error_, Critical_;

#define __FILENAME__ \
    (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define LOG_STR_H(x) #x
#define LOG_STR(x) LOG_STR_H(x)

#define LOG_TOKEN(fmt) fmt " (" LOG_STR(__FILE__) " #" LOG_STR(__LINE__) ")"

#define Trace(fmt, ...) \
    Trace_->trace(LOG_TOKEN(fmt), ##__VA_ARGS__)
#define Debug(fmt, ...) \
    Debug_->debug(LOG_TOKEN(fmt), ##__VA_ARGS__)
#define Info(fmt, ...) \
    Info_->info(LOG_TOKEN(fmt), ##__VA_ARGS__)
#define Warn(fmt, ...) \
    Warn_->warn(LOG_TOKEN(fmt), ##__VA_ARGS__)
#define Error(fmt, ...) \
    Error_->error(LOG_TOKEN(fmt), ##__VA_ARGS__)
#define Critial(fmt, ...) \
    Critical_->critical(LOG_TOKEN(fmt), ##__VA_ARGS__)

void config(const std::string& config_file);
