#pragma once

#include <asio/io_service.hpp>

namespace ICore {
class IService;
}

class ICore::IService {
public:
    virtual ~IService() = default;

    virtual void readConfig(const std::string& config_file) noexcept = 0;

    virtual asio::io_service& service(size_t n) noexcept = 0;
};
