#pragma once

#include "class_def.h"
#include "iobject.h"
#include <memory>
#include <pugixml.hpp>
#include <vector>

namespace ICore {
class Object;
}

class ICore::Object : public ICore::IObject, public std::enable_shared_from_this<Object> {
    DEC_CMA(Object)
    Object();
    Object(const Object& object);

public:
    id_type id() const noexcept override;
    virtual void setId(id_type id) noexcept override;
    shared_type child(const id_type& id) noexcept override;
    virtual void setParameter(const pugi::xml_node& node) noexcept override;
    virtual shared_type createChild(const pugi::xml_node& node) noexcept override;
    virtual void start() noexcept override;
    virtual void stop() noexcept override;
    std::string name() const override;
    void config(const pugi::xml_node& node) noexcept override;
    void addChild(shared_type child) noexcept override;
    shared_type releaseChild(id_type id) noexcept override;
    virtual void setParent(pointer_type parent) noexcept override;
};
