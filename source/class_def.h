#pragma once
#include <memory>

#define DEC_CMA(T)               \
    struct Impl;                 \
    std::unique_ptr<Impl> pImpl; \
                                 \
public:                          \
    T(T&& t);                    \
    T& operator=(const T& rhs);  \
    T& operator=(T&& rhs);       \
    virtual ~T() override;

#define DEF_CMA(T)                         \
    T::T(T&& t) = default;                 \
    T& T::operator=(const T& rhs)          \
    {                                      \
        pImpl.reset(new Impl(*rhs.pImpl)); \
        return *this;                      \
    }                                      \
    T& T::operator=(T&& rhs) = default;    \
    T::~T() = default;
