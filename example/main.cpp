#include "../source/logger.h"
#include "../source/service.h"
#include <asio/signal_set.hpp>
#include <asio/steady_timer.hpp>

std::unique_ptr<ICore::Service> service;

void handler(const asio::error_code& error, int signal_number)
{
    Warn("Received signal {}", signal_number);
    if (!error) {
        service->stop();
    }
}


int main(int argc, char* argv[])
{
    config("config.xml");

    service.reset(new ICore::Service());

    service->readConfig("config.xml");
    Error("start");

    asio::signal_set sigs(service->service(0), SIGUSR2, SIGINT);
    //    asio::signal_set sigs1(service->service(1), SIGUSR2, SIGINT);

    Debug("This thread {}", std::this_thread::get_id());
    sigs.async_wait(handler);
    //    sigs1.async_wait(handler);

    //      std::vector<std::unique_ptr<asio::steady_timer>> timers;

    //      for(int i = 0; i < 10; ++i) {
    //          auto timer = std::make_unique<asio::steady_timer>(service->service(i %
    //          2));
    //          timer->expires_from_now(std::chrono::seconds(3));
    //          timer->async_wait(
    //                std::bind(&Service::print, service, std::placeholders::_1));
    //          timers.push_back(std::move(timer));
    //        }
    //    std::error_code ec;
    //    service->print(ec);
    service->start();

    service->stop();

    //    service.reset(nullptr);

    Error("Now quiting");
    return 0;
}
