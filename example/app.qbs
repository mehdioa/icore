import qbs
import "../../preset.js" as Preset

Product {
    type: "application"
    name: "icore_example"
    version: "1"
    files:[
        "*.h",
        "*.cpp",
    ]

    Depends {name: "cpp"}

    cpp.cxxLanguageVersion: project.cxxLanguageVersion
    cpp.warningLevel: project.warningLevel

//    cpp.cxxFlags:{
//            var flags = base
//            if(cpp.compilerName.contains("g++") || cpp.compilerName.contains("gcc"))
//                flags = flags.concat(["-std=gnu++11"])
//            return flags
//        }

//    cpp.linkerFlags: {
//        if (qbs.buildVariant == "release" && (qbs.toolchain.contains("gcc") || qbs.toolchain.contains("mingw")))
//            return ["-s", "-O3", "-g3", "-Wno-deprecated", "-fpermissive"]
//    }

    cpp.defines: {
        var defs = ["ASIO_STANDALONE"]
        if (qbs.buildVariant == "release") {
            defs.push("LOG_NO_NAME")
            defs.push("RELEASE_MODE")
        }
        return defs
    }

    cpp.libraryPaths: [project.LIB_DIR]

//    cpp.staticLibraries: {
//        var sl = ["icore", "pugixml"]
//        if (qbs.targetOS.contains("windows"))  {
////            sl.push("icore")
//        }
//        return sl
//    }

    cpp.dynamicLibraries: {
        var sl = [Preset.setName(qbs.buildVariant, "icore"), "pugixml"]
        if (qbs.targetOS.contains("unix"))  {
            sl.push("pthread")
        }

        return sl
    }

    cpp.includePaths: {
        var ip = []
        if (qbs.targetOS.contains("windows")) {
            ip.push(project.INC_DIR)
        }
        return ip
    }

    Group {
        condition: qbs.targetOS.contains("unix") && !qbs.targetOS.contains("osx")
        qbs.install: true
        qbs.installDir: "local/share/icore/examples"
        fileTagsFilter: product.type
    }

    Group {
        condition: qbs.targetOS.contains("unix") && !qbs.targetOS.contains("osx")
        qbs.install: true
        qbs.installDir: "local/share/icore/examples"
        files: [
            "config.xml",
        ]
    }
}

