import qbs
import qbs.Environment

Project {
    name: "ICore"
    property int major: 0
    property int minor: 1
    property int patch: 0
    property string version: major+"."+minor+"."+patch
    property string cxxLanguageVersion: "c++17"
    property string warningLevel: "all"

    property string INSTALL_PATH: "/usr/local"
    property bool BUILD_TEST: true
    property bool BUILD_EXAMPLES: true
    property string LIB_DIR:  INSTALL_PATH+"/lib"
    property string INC_DIR:  INSTALL_PATH+"/include"
    property string LIB_TYPE:  "dynamiclibrary"

    references: {
        ref = ["source/app.qbs"];
        if (BUILD_TEST) {
            ref.push("test/app.qbs")
        }
        if (BUILD_EXAMPLES) {
            ref.push("example/app.qbs")
        }
        return ref
    }
}
