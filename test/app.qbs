import qbs
import "../../preset.js" as Preset

Product {
    type: "application"
    name: "test"
    version: "1"
    Depends {name: "cpp"}
    files:[
        "*.h",
        "*.cpp",
    ]

    cpp.cxxLanguageVersion: project.cxxLanguageVersion
    cpp.warningLevel: project.warningLevel

    Depends { name: "cpp" }

    cpp.staticLibraries: [
        Preset.setName(qbs.buildVariant, "icore"),
        "pthread",
        "gtest_main",
        "gtest"
    ]

    cpp.includePaths: [
        ".",
        "../source"
    ]
    cpp.defines: [
        "RAPIDJSON_HAS_STDSTRING",
    ]

    Group {
        condition: qbs.targetOS.contains("unix") && !qbs.targetOS.contains("osx")
        qbs.install: true
        qbs.installDir: "local/share/icore/tests"
        fileTagsFilter: product.type
    }

    Group {
        condition: qbs.targetOS.contains("unix") && !qbs.targetOS.contains("osx")
        qbs.install: true
        qbs.installDir: "local/share/icore/tests"
        files: [
            "config.xml",
        ]
    }
}

