#include "../source/object.h"
#include "../source/logger.h"
#include "../source/service.h"
#include <gtest/gtest.h>

using namespace ICore;

class ConfigFixture : public ::testing::Test {
protected:
    virtual void SetUp()
    {
        config("config.xml");
    }

    virtual void TearDown()
    {
        spdlog::drop_all();
    }
};

namespace {

TEST_F(ConfigFixture, Creation)
{
    Object object;
    EXPECT_EQ(object.id(), 0);
}
TEST_F(ConfigFixture, RuleOfFive)
{
    Service service;
    service.readConfig("config.xml");

    EXPECT_EQ(service.id(), 0);
    EXPECT_EQ(service.name(), "RootObject");
}
}

//struct Fixture {
//    Fixture()
//    {
//        config("config.xml");
//    }

//    ~Fixture()
//    {
//        spdlog::drop_all();
//    }
//};

//TEST_CASE_METHOD(Fixture, "Root id is zero", "[object]")
//{
//    Object object;

//    REQUIRE(object.id() == 0);
//}

//TEST_CASE_METHOD(Fixture, "Rule of five", "[object]")
//{
//    Service service;
//    service.config("config.xml");

//    REQUIRE(service.id() == 0);
//    REQUIRE(service.name() == "RootObject");
//    REQUIRE(service.name() == "RootObject");
//}
